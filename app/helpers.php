<?php

use App\Models\User;
use ReallySimpleJWT\Build;
use ReallySimpleJWT\Token;
use ReallySimpleJWT\Helper\Validator;
use ReallySimpleJWT\Encoders\EncodeHS256;

function current_user(): User
{
    $token = request()->cookie('token');
    return User::find(Token::getPayload($token)['id']);
}

function createToken($user_id)
{
    $build = new Build('JWT', new Validator(), new EncodeHS256(env('JWT_SECRET')));
    return $build->setContentType('JWT')
        ->setIssuer($_SERVER['SERVER_NAME'] ?? "")
        ->setSubject('users')
        ->setExpiration(time() + 3153600000)
        ->setIssuedAt(time())
        ->setPayloadClaim('id', $user_id)
        ->build()
        ->getToken();
}
