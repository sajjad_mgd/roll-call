<?php

namespace App\Http\Middleware;

use ReallySimpleJWT\Token;
use Illuminate\Http\Request;

class CustomAuthenticate
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, \Closure $next)
    {
        $token = $request->cookie('token');
        try {
            $lastRoute = url()->current();
            $tokenIsValid = Token::validate($token, env('JWT_SECRET'));
            $user = current_user();
            if ($tokenIsValid && ($user->isAdmin() xor !str_starts_with($request->path(), env('APP_PREFIX') ? env('APP_PREFIX') . '/admin' : 'admin'))) {
                view()->share('currentUser', $user);
                $token = createToken($user->id);
                return $next($request)->withCookie(cookie('token', $token, 60));
            }
            return redirect()->route('auth.show-login')->withCookie(cookie(
                'lastRoute',
                $lastRoute,
                time() + (10 * 365 * 24 * 60 * 60)
            ));
        } catch (\Throwable) {
            return redirect()->route('auth.show-login')->withCookie(cookie(
                'lastRoute',
                $lastRoute,
                time() + (10 * 365 * 24 * 60 * 60)
            ));
        }
    }
}
