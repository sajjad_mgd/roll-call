<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RollCallRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'unique_code' => 'required|exists:participant,unique_code'
        ];
    }
}
