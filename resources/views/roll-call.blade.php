<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>انجمن اسلامی دانشجویان دفتر تحکیم وحدت دانشگاه تبریز</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <style>
        body {
            background-color: #ebebff
        }

        .card {
            width: 700px;
            background-color: #fff;
            border: none;
            border-radius: 12px
        }

        .form-control {
            margin-top: 10px;
            height: 48px;
            border: 2px solid #eee;
            border-radius: 10px
        }

        .form-control:focus {
            box-shadow: none;
            border: 2px solid #039BE5
        }
    </style>
    @vite('resources/js/app.jsx')
</head>

<body>
    <div class="container mt-5 mb-5 d-flex justify-content-center">
        <div class="card px-1 py-4">
            <h2 class="text-center mb-3">{{ $meeting->title }}</h2>
            <div id="code-scanner" csrf-token="{{ csrf_token() }}"></div>
        </div>
    </div>
</body>

</html>
