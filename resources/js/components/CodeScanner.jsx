// resources/js/components/HelloReact.js

import React from 'react';
import { createRoot } from 'react-dom/client'

export default function HelloReact() {
    return (
        <h1>Hello React!</h1>
    );
}

if (document.getElementById('code-scanner')) {
    createRoot(document.getElementById('code-scanner')).render(<HelloReact />);
}
